import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    films: [],
    people: [],
    starships: []
  },
  getters: {
    getCollections: state => ({ searchstr, collection }) => {
      //get all result what matching search query( by default all )
      const col = collection === "films" ? "title" : "name";
      return state[collection].filter(item => {
        return item[col].toLowerCase().match(searchstr.toLowerCase());
      })
    },
    getItemFromCollection: state => ({ id, collection }) => {
      //get info about single item from collection
      let match = [];
      if (collection === "films") {
        state[collection].find(item => {
          item.episode_id === id ? match.push(item) : null
        })
      } else {
        state[collection].find(item => {
          const currId = parseInt(/\d{1,2}/.exec(item.url));
          currId === id ? match.push(item) : null
        });
      }
      return match
    },
    getFilmsById: state => id => {
      //get film title
      let match = [];
      id.forEach(itemid => {
        state.films.find(item => {
          if (item.episode_id === itemid) {
            match.push({
              title: item.title,
              routes: itemid
            })
          }
        });
      })
      return match;
    },
    getShipsOrPeoplesById: state => ({ id, collection }) => {
      //get ship or person name
      let match = [];
      id.forEach(itemid => {
        state[collection].find(item => {
          const currId = parseInt(/\d{1,2}/.exec(item.url));
          if (currId === itemid) {
            match.push({
              name: item.name,
              routes: itemid
            })
          }
        });
      });
      return match;
    }
  },
  actions: {
    async getDataApi({ state }, name) {
      let response = await fetch(`https://swapi.co/api/${name}/`);
      let jsonResponse = await response.json();
      //get count of remaining pages( 10 item per page )
      let count = Math.floor(jsonResponse.count / 10);
      state[name] = [...jsonResponse.results];
      //check for films 
      if (count !== 0) {
        for (let i = 2; i < count; i++) {
          let pageresponse = await fetch(`https://swapi.co/api/${name}/?page=${i}`);
          let jsonResponse2 = await pageresponse.json();
          state[name].push(...jsonResponse2.results);
        }
      }
    }
  }
})