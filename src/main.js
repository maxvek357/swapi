import Vue from 'vue';
import VueRouter from 'vue-router';

import App from './App.vue';
import Film from './components/Film';
import Person from './components/Person';
import Ship from './components/Ship';
import PageNotFound from './components/PageNotFound';

import { store } from './store';


Vue.use(VueRouter);

const routes = [
  { path: '/', component: Film },
  { path: '/film/', component: Film },
  { path: '/film/:filmId', component: Film },
  { path: '/person', component: Person },
  { path: '/person/:personId', component: Person },
  { path: '/ship', component: Ship },
  { path: '/ship/:shipId', component: Ship },
  { path: '*', component: PageNotFound }
];

const router = new VueRouter({
  routes,
  mode: 'history'
})


new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app');
